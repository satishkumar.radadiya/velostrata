#!/bin/bash

#adding a user searce with sudo priviledge
/usr/sbin/useradd searce
/usr/sbin/usermod -aG sudo searce
read -p "Enter the password for user searce" password
echo "$password" | /usr/bin/passwd searce --stdin
sleep 5s

# download and install velostrata prep package
/usr/bin/curl -o velostrata_prep.deb https://storage.googleapis.com/velostrata-release/V4.2.0/Latest/velostrata-prep_4.2.0.deb
/usr/bin/dpkg -i velostrata_prep.deb
/usr/bin/apt-get install -f -y
/usr/bin/apt-add-repository universe
/usr/bin/apt update
/usr/bin/apt install -y google-compute-engine google-osconfig-agent
sleep 10s

# restarting the VM
/sbin/init 6
